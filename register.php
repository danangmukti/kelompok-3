<?php

require 'connect.php';

if (isset($_POST["register"])) {

    if (registrasi($_POST) > 0) {
?>

        <script>
            alert('user baru berhasil ditambahkan!')
            window.location.replace("login.php");
        </script>;
<?php
    } else {
        echo mysqli_error($conn);
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Optimus | Register</title>
    <link href="plant.png" rel="icon">
    <link href="assets/img/Logo-icon.png" rel="apple-touch-icon">

    <!--fonts-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- styles-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Boxicons -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- My Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"/>

</head>

<body style="background-color: ; background-position: top;">
        <div class="container justify-content-center col-sm-6 col-lg-5">
            <div class="header mt-5 text-center" style="background-color:#D8E9A8; border-radius:15px; height:180px;">
                <br>
                <img src="img/logo-icon.png" alt="" style="width: 300px;">
            </div>  
            <div>
                <!-- Pills navs -->
                <ul class="nav nav-pills nav-justified mb-5 mt-3" id="ex1" role="tablist">
                    <li class="nav-item" role="presentation" style>
                        <a class="nav-link" id="tab-login" data-mdb-toggle="pill" href="login.php" role="tab"
                        aria-controls="pills-login" aria-selected="false">Login
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" style="background-color: #1E5128;" id="tab-register" data-mdb-toggle="pill" href="#pills-register" role="tab"
                        aria-controls="pills-register" aria-selected="true" style="color: #1E5128;">Register</a>
                    </li>
                </ul>
                <!-- Pills navs -->
                <div class="tab-content">
                
                    <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="tab-login">
                        <form action="" method="POST">
                        <div class="text-center mb-4">
                            <p>Buat Akun :</p>
                        </div>

                        <!-- Email input -->
                        <div class="form-outline mb-4">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Masukkan Email" required>
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4 row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" id="password" name= "password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" id="password2" name= "password2" class="form-control" placeholder="Konfirmasi Password" required>
                            </div>
                        </div>
                        
                        <div class="text-center">
                            <button type="submit" class="btn btn-success mb-4 col-lg-4 text-center" name = "register" id = "register"><i class='fa fa-pencil-square-o'></i> Registrasi Akun</button>
                            <br>
                            <p class="mb-4">Sudah Punya Akun? <a href="#!"> Login! </a></p>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>