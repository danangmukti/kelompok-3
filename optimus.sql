-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2022 at 04:24 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `optimus`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `email`, `password`) VALUES
(30, 'anja@gmail.com', '$2y$10$xCVyA6nwsMy3CkqRAd5pWOIVNTV.huUkKPhXJMHu3tMBXtsa0QZwq'),
(31, 'danang@admin.com', '$2y$10$1fViCUthTgn0T9dBQyAeMO1QvE/t.YXWaZcKdMu6f8kaFhS5FYunK'),
(32, 'rizhanmahardika@gmail.com', '$2y$10$ZOcmFVD3YaaH5QbrPIL5TueQ8sdJoTvbhedRDGC3kWbwxqs4IDXc2'),
(33, 'asd@a', '$2y$10$atXKIGPtU7o.xKYGpKkLtuhAgLU80G31p0jTjVM7bhnXsfgC5NsBS');

-- --------------------------------------------------------

--
-- Table structure for table `kirimpesan`
--

CREATE TABLE `kirimpesan` (
  `id_message` int(255) NOT NULL,
  `to` varchar(20) NOT NULL,
  `from` varchar(20) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kirimpesan`
--

INSERT INTO `kirimpesan` (`id_message`, `to`, `from`, `message`) VALUES
(6, 'siapa hayo', 'saya', 'kenapa yaa gitu gitu wae');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `pesan` int(11) NOT NULL,
  `isikomentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `konsul`
--

CREATE TABLE `konsul` (
  `id_pasien` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `keluhan` varchar(200) NOT NULL,
  `konsumsi` varchar(100) NOT NULL,
  `gejala` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `konsul`
--

INSERT INTO `konsul` (`id_pasien`, `email`, `nama`, `keluhan`, `konsumsi`, `gejala`) VALUES
(1, 'danang@admin.com', 'iya', 'panas', 'ya', '5 hari'),
(2, 'danang@admin.com', 'danang', 'panas', 'ya', '1'),
(3, 'danang@admin.com', 'iya', 'panas', 'ya', '5 hari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `kirimpesan`
--
ALTER TABLE `kirimpesan`
  ADD PRIMARY KEY (`id_message`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `konsul`
--
ALTER TABLE `konsul`
  ADD PRIMARY KEY (`id_pasien`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `kirimpesan`
--
ALTER TABLE `kirimpesan`
  MODIFY `id_message` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `konsul`
--
ALTER TABLE `konsul`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
