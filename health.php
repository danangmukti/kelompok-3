<!DOCTYPE html>
<html>
<head>
	<title>Optimus | Health</title>
	<link rel="icon" href="plant.png" style="width: 100%;">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/health.css">
	<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">


	<style>
		.wow:first-child {
		visibility: hidden;
		}
	</style>
  
</head>
<body>
 
	<!-- Start Header  -->
	<header>
		<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-danger word">
			<div class="container">
				<a class="navbar-brand" href="./index.php">
				<img src="img/transparent-arrow-icon-back-icon-backwards-icon-5d6564c1a5aef6.7873536615669260176786.png" alt="" width="30" height="24">
				</a>
				<div class="logo">
					<a class="" href="./index.php">
						<i class='back'></i>
					</a>
					<a href="">Kesehatan</a>
				</div>
			</div>
		</nav>
	</header>
	<!-- End Header  -->

	<!-- Start Home -->
	<section class="home wow flash" id="home">
		<div class="container">
			<h1 class="wow slideInLeft" data-wow-delay="1s">Saatnya <span>olahraga</span> </h1>
			<h1 class="wow slideInRight" data-wow-delay="1s">Waktunya <span> bergerak</span></h1>
		</div>
		<!-- go down -->
			<a href="#about" class="go-down">
				<i class="fa fa-angle-down" aria-hidden="true"></i>
			</a>
		<!-- go down -->
	</section>
	<!-- End Home -->

	<!-- Start Service -->
	<section class="service" id="service">
		<div class="container">
				<div class="content">
					<div class="text box wow slideInLeft">
						<h2>4 Sehat 5 Sempurna</h2>
						<p>
							Makanan 4 sehat 5 sempurna adalah menu makanan yang lengkap dan mengandung zat gizi yang dibutuhkan oleh tubuh seperti karbohidrat, protein, vitamin dan mineral. 
							Masing-masing zat gizi ini terkandung dalam berbagai jenis makanan yang berbeda.
						</p>
						<p>
							Makanan 4 sehat terdiri atas makanan pokok, lauk pauk, sayur, dan buah. Sedangkan 5 sempurna merupakan susu yang merupakan nutrisi tambahan.  Awalnya makanan 4 sehat dan 5 sempurna ini 
							merupakan kampanye yang digalakkan pemerintah namun saat ini sudah menjadi gaya hidup sebagian orang. 
						</p>
					</div>
					<div class="accordian box wow slideInRight">
						<div class="accordian-container active">
							<div class="head">
								<h4>Makanan Pokok</h4>
								<span class="fa fa-angle-down"></span>
							</div>
							<div class="body">
								<p>
									Makanan pokok adalah makanan yang dikonsumsi dalam porsi yang banyak, menjadi sumber karbohidrat, 
									memiliki rasa yang netral, mengenyangkan, dan didapatkan dari hasil alam daerah setempat.
								</p>
							</div>
						</div>
						<div class="accordian-container">
							<div class="head">
								<h4>Lauk Pauk</h4>
								<span class="fa fa-angle-up"></span>
							</div>
							<div class="body">
								<p>
									Lauk pauk merupakan suatu hidangan pelengkap nasi yang dapat berasal dari bahan hewan serta produknya, 
									tumbuh-tumbuhan atau kombinasi antara bahan hewan dan tumbuh-tumbuhan yang mana biasanya dimasak dengan suatu bumbu tertentu. 
									Contoh bahan hewani yaitu, daging, ayam, unggas, jeroan, serta ikan.
								</p>
							</div>
						</div>
						<div class="accordian-container">
							<div class="head">
								<h4>Buah-Buahan</h4>
								<span class="fa fa-angle-up"></span>
							</div>
							<div class="body">
								<p>
									Buah-buahan merupakan sumber berbagai vitamin (Vit A, B, B1, B6, C), mineral, dan serat pangan.
									Sebagian vitamin, mineral yang terkandung dalam buah-buahan berperan sebagai anti oksidan.
								</p>
							</div>
						</div>
						<div class="accordian-container">
							<div class="head">
								<h4>Sayur-Sayuran</h4>
								<span class="fa fa-angle-up"></span>
							</div>
							<div class="body">
								<p>
									Sayur adalah semua jenis tanaman yang dapat dikonsumsi baik yang diambil dari akar, batang, daun, biji, bunga atau bagian lain yang digunakan untuk diolah menjadi masakan. 
									Sayuran merupakan sebutan umum bagi bahan pangan asal tumbuhan yang biasanya mengandung kadar air tinggi dan dikonsumsi dalam keadaan segar atau setelah diolah secara minimal
								</p>
							</div>
						</div>
					</div>
				</div>
		</div>
		</section>
		<!-- End Service -->
	
		<!-- Start Today -->
		<section class="start-today" style="background-color: #1E5128">
		<div class="container">
				<div class="content">
					<div class="box img wow slideInLeft" style="margin-right: 100px;">
						<img src="img/piring-ideal.png" alt="start today" />
					</div>
					<div class="box text wow slideInRight">
						<h2 style="color: #D8E9A8;">Porsi Makanan Ideal</h2>
						<p><span style="color: #D8E9A8; font-weight: 50;">Dalam makanan kita harus membagi porsi makanan, </span><span style="color: rgb(255, 242, 53);">30% Makanan Pokok</span>, <span style="color: rgb(253, 138, 138);">20% Lauk Pauk</span>, <span style="color: rgb(241, 132, 255);">20% Buah-buahan</span>, <span style="color: rgb(102, 255, 140);">30% Sayuran </span></p>
					</div>
				</div>
		</div>
		</section>
		<!-- End Start Today -->


	<!-- Start About -->
	<section class="about" id="about">
		<div class="container">
			<div class="content">
				<div class="box wow bounceInUp">
					<div class="inner">
						<div class="img">
							<img src="img/buah.jpg" alt="about" />
						</div>
						<div class="text text-left">
							<h4><a href="https://www.blibli.com/friends/blog/catat-ini-dia-cara-memilih-buah-yang-segar/" style="text-decoration: none; color: #b5fcaf;">Pemilihan Buah yang Baik</a></h4>
							<p>Buah yang baik merupakan sebuah syarat penting untuk dapat dikonsumsioleh manusia. Buah yang baik tidak hanya dilihat dari bentuk nya saja,  pada card inilah akan dijelaskan pemilihan buah yang baik.</p>
						</div>
					</div>
				</div>
				<div class="box wow bounceInUp" data-wow-delay="0.2s">
					<div class="inner">
						<div class="img">
							<img src="img/rebus.jpg" alt="about" />
						</div>
						<div class="text">
							<h4><a href="https://www.rskariadi.co.id/news/102/MENGOLAH-BUAH-DAN-SAYUR-DENGAN-BENAR/Artikel" style="text-decoration: none; color: #b5fcaf;">Cara Mengolah Buah dan Sayur</a></h4>
							<p>Tahapan mengolah buah dan sayur tidak kalah penting dengan cara pemilihannya. Buah dan sayur akan awet dan teraga nutrisi nya ketika di olah dengan benar agar nutrisi tetap terjaga walau sudah di simpan lama.</p>
						</div>
					</div>
				</div>
				<div class="box wow bounceInUp" data-wow-delay="0.4s">
					<div class="inner">
						<div class="img">
							<img src="img/nyayur.jpg" alt="about" />
						</div>
						<div class="text text-left">
							<h4><a href="https://www.gramedia.com/best-seller/cara-memilih-sayuran-yang-baik/" style="text-decoration: none; color: #b5fcaf;">Pemilihan Sayur yang Baik</a></h4>
							<p>Tidak semua sayur yang terlihat segar itu baik untuk di konsumsi. Ketika pengolahan sayur salah, maka nutrisi yang ada di dalam sayur akan berkurang. Pada Card ini akan dijelaskan mengenai ciri dan cara untuk mengolah sayur dengan benar. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End About -->

	<!-- Start Today -->
	<section class="start-today">
		<div class="container">
			<div class="content">
				<div class="box text wow slideInLeft">
						<h2 style="color: #004105;">Pentingnya Cairan Tubuh</h2>
						<p style="color: #004105;">Dengan memenuhi kebutuhan cairan tubuh, sistem kekebalan tubuh pun akan menjadi lebih kuat. 
							Air membawa oksigen pada sel-sel tubuh, sehingga sistem menjadi lancar. 
							Di samping itu, air pun bisa membersihkan racun berbahaya dari tubuh karena kinerja ginjal menjadi lancar dan baik.
						</p>
				</div>
				<div class="box img wow slideInRight" style="margin-right: 50px;">
						<img src="img/kebutuhan-air.png" alt="start today" />
				</div>

			</div>
		</div>
	</section>
	<!-- End Start Today -->

	<!-- Start Classes -->
	<section class="start-today">
	<div class="container">
			<div class="content">
				<div class="box img wow slideInLeft">
					<img src="img/food.png" alt="classes" style="width: 80%; margin-left: 50px;"/>
				</div>
				<div class="box text wow slideInRight" style="margin-top: 50px; margin-left: 25px;">
					<h2>Pentingkah Memperhatikan Apa Yang Dikonsumsi ?</h2>
					<p>Gizi adalah zat makanan pokok yang diperlukan bagi pertumbuhan dan kesehatan tubuh. Gizi seimbang adalah susunan makanan sehari-hari yang mengandung zat gizi dalam jenis dan jumlah yang sesuai dengan kebutuhan tubuh yaitu jenis kelamin, umur dan status kesehatan. Pola makan yang tidak bergizi seimbang beresiko menyebabkan kekurangan gizi seperti anemia dan berat badan kurang, dapat pula terjadi gizi berlebih (obesitas) yang dapat beresiko terjadinya penyakit degeneratif seperti hipertensi, penyakit jantung koroner dan diabetes melitus.</p>
				</div>
			</div>
	</div>
	</section>
	<!-- End Classes -->


	<!-- Start Today -->
	<section class="start-today">
	<div class="container">
			<div class="content">
				<div class="box text wow slideInLeft">
					<h2 >Kenapa Konsumsi Vitamin ?</h2>
					<p >Peranan vitamin dan mineral adalah untuk mencegah agar imunitas kita tidak menurun, sehingga dapat menangkal terjadinya komplikasi akibat COVID-19. 
					   Vitamin dan mineral tetap diperlukan untuk menunjang kinerja tubuh kita meskipun kita sedang beraktivitas di rumah saja</p>
				</div>
				<div class="box img wow slideInRight" style="margin-right: 50px;">
					<img src="img/vitamin.png" alt="start today" />
				</div>

			</div>
	</div>
	</section>
	<!-- End Start Today -->

	<!-- Start Schedule -->
	<section class="schedule" id="schedule">
		<div class="container">
			<div class="content" style="background-color:white; font-color:black; border-radius:15px;">
				<div class="box text wow slideInLeft">
					<h2 style="color: black; font-weight:bold; margin-left:15px; margin-top:15px;">Kontrol Asupan Makanan</h2>
					<img src="img/change.png" alt="schedule" style="width: 80%; margin-left: 50px;" />
					<p style="color: black; font-size:14px; margin-left:15px;">
						Selain kurangnya latihan fisik, sebagian besar masalah kesehatan dipicu oleh konsumsi makanan yang tidak benar. Mengurangi asupan makanan, atau diet menjadi cara kebanyakan orang untuk menurunkan berat badan. Meski kadang, cara itu juga tidak efektif.
						Perlu diketahui, kunci keberhasilan menurunkan berat badan sebenarnya bukan mengurangi asupan makanan, melainkan mengonsumsi makanan yang tepat untuk mempercepat metabolisme tubuh. 
					</p>
				</div>
				<div class="box timing wow slideInRight">
					<table class="table table-dark table-bordered table-striped cell-border" id="data">
						<thead>
							<th>Waktu</th>
							<th>Selang Waktu</th>
							<th>Saran</th>
						</thead>
						<tbody style="color: black;">
							<tr>
								<td>Pagi</td>
								<td>06:00 - 09:45</td>
								<td>Makan</td>
							</tr>
							<tr>
								<td>Pagi</td>
								<td>Setelah Sarapan <br/>Selang 2 s.d. 4 jam</td>
								<td>Ngemil</td>
							</tr>
							<tr>
								<td>Siang</td>
								<td>12:00 - 13:45</td>
								<td>Makan</td>
							</tr>
							<tr>
								<td>Sore</td>
								<td>Setelah Makan Siang <br/>Selang 2 s.d. 4 jam</td>
								<td>Ngemil</td>
							</tr>
							<tr>
								<td>Malam</td>
								<td>Sebelum <br/> 19:00</td>
								<td>Makan</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	<!-- End Schedule -->

	<!-- Start Today -->
	<section class="start-today" style="background-color: #D8E9A8;">
		<div class="container">
			<div class="content">
				<div class="box img wow slideInLeft" style="margin-right: 100px; ">
					<img src="img/waktu-minum.png" alt="start today" />
				</div>
				<div class="box text wow slideInRight">
					<h2>Penuhi Kebutuhan Mineral Tubuh</h2>
					<p>Secara umum kebutuhan mineral dalam tubuh sangat sedikit yaitu ± 4% dari total berat badan kita. Walaupun demikian mineral tidak dapat diabaikan begitu saja karena tanpa mineral essential kehidupan kita sulit untuk dipertahankan.</p>
				</div>
			</div>
		</div>
	</section>

	<!-- jquery -->
	<script src="js/wow.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/searchpanes/2.1.0/js/dataTables.searchPanes.min.js"></script>
	<script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
	<script>
		$(document).ready(function(){

		$(".ham-burger, .nav ul li a").click(function(){
		
			$(".nav").toggleClass("open")

			$(".ham-burger").toggleClass("active");
		})      
		$(".accordian-container").click(function(){
			$(".accordian-container").children(".body").slideUp();
			$(".accordian-container").removeClass("active")
			$(".accordian-container").children(".head").children("span").removeClass("fa-angle-down").addClass("fa-angle-up")
			$(this).children(".body").slideDown();
			$(this).addClass("active")
			$(this).children(".head").children("span").removeClass("fa-angle-up").addClass("fa-angle-down")
		})

		$(".nav ul li a, .go-down").click(function(event){
			if(this.hash !== ""){

				event.preventDefault();

				var hash=this.hash; 

				$('html,body').animate({
					scrollTop:$(hash).offset().top
				},800 , function(){
					window.location.hash=hash;
				});

				// add active class in navigation
				$(".nav ul li a").removeClass("active")
				$(this).addClass("active")
			}
		})
	})
	</script>
	<script>
		wow = new WOW(
		{
			animateClass: 'animated',
			offset:       0,
		}
		);
		wow.init();
	</script>
	<script>
		$(document).ready(() => {
			var table = $('#data').DataTable({
				dom : 'PlfBrtip',
				lengthChange: false,
        		buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
			});
			
			table.buttons().container()
        		.appendTo( '#example_wrapper .col-md-6:eq(0)' );

			var container = $('<div/>').insertBefore(table.table().container());

			var chart = Highcharts.chart(container[0], {
				chart: {
					type: 'pie',
				},
				title: {
					text: 'Waktu Konsumsi',
				},
				series: [
					{
						data: chartData(table),
						name: 'Aktivitas',
					},
				],
			});

			// On each draw, update the data in the chart
			table.on('draw', function () {
				chart.series[0].setData(chartData(table));
			});
		});

		function chartData(table) {
			var counts = {};
		
			// Count the number of entries for each position
			table
				//Menentukan apa yang dibuat menjadi grafik
				.column(0, { search: 'applied' })
				.data()
				.each(function (val) {
					if (counts[val]) {
						counts[val] += 1;
					} else {
						counts[val] = 1;
					}
				});
		
			// And map it to the format highcharts uses
			return $.map(counts, function (val, key) {
				return {
					name: key,
					y: val,
				};
			});
		}
	</script>
	

</body>
</html>






