<?php
require 'connect.php';
session_start();

//cek cookie
if (isset($_COOKIE['id']) && isset($_COOKIE['key'])) {
    $id = $_COOKIE['id'];
    $key = $_COOKIE['key'];

    //ambil username berdasarkan id
    $result = mysqli_query($conn, "SELECT email FROM akun WHERE id = $id");
    $row = mysqli_fetch_assoc($result);

    //cek cookie dan username
    if ($key === hash('sha256', $row['email'])) {
        $_SESSION['login'] = true;
    }
}


if (isset($_SESSION["login"])) {
    header("Location: index.php");
    exit;
}



if (isset($_POST["login"])) {

    $email = $_POST["email"];
    $password = $_POST["password"];

    $result = mysqli_query($conn, "SELECT * FROM akun WHERE email = '$email'");

    //cek username
    if (mysqli_num_rows($result) === 1) {

        //cek password
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row["password"])) {
            //cek session
            $_SESSION["login"] = true;

            //cek remember me
            if (isset($_POST['remember'])) {
                //buat cookie
                setcookie('id', $row['id_akun'], time() + 60);
                setcookie('key', hash('sha256', $row['email']), time() + 60);
            }


            header("Location: index.php");
            exit;
        }
    }
    $error = true;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Optimus | Login</title>
    <link href="plant.png" rel="icon">
    
    <!-- Boxicons -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- My Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"/>
</head>

<body style="background-color: ; background-position: top;">
  <div class="container justify-content-center col-sm-6 col-lg-5">
    <div class="header mt-5 text-center" style="background-color:#D8E9A8; border-radius:15px; height:180px;">
      <br>
      <img src="img/logo-icon.png" alt="" style="width: 300px;">
    </div>
    <div class="">
        <!-- Pills navs -->
        <ul class="nav nav-pills nav-justified mb-5 mt-3" id="ex1" role="tablist">
        <li class="nav-item" role="presentation" style>
            <a class="nav-link active" style="background-color: #1E5128;" id="tab-login" data-mdb-toggle="pill" href="#pills-login" role="tab"
            aria-controls="pills-login" aria-selected="true">Login
            </a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="tab-register" data-mdb-toggle="pill" href="register.php" role="tab"
            aria-controls="pills-register" aria-selected="false">Register</a>
        </li>
        </ul>
        <!-- Pills navs -->

        <!-- Pills content -->
        <div class="tab-content">
        <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="tab-login">
            <?php if (isset($error)) : ?>
                <p style="color:red; font-style:italic;">Email / Password Salah, Cek Sekali Lagi</p>
            <?php endif; ?>
            <form action="" method="POST">
            <div class="text-center mb-2">
                <p>Sign in with :</p>
            </div>

            <!-- Email input -->
            <div class="form-outline mb-4">
                <input type="email" id="email" name="email" class="form-control" />
                <label class="form-label" for="email">Email</label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <input type="password" id="password" name= "password" class="form-control" />
                <label class="form-label" for="password">Password</label>
            </div>

            <!-- 2 column grid layout -->
            <!-- <div class="row mb-4">
                <div class="col-md-6 d-flex justify-content-center">
                <div class="form-check mb-3 mb-md-0">
                    <input class="form-check-input" type="checkbox" value="" id="loginCheck" checked />
                    <label class="form-check-label" for="loginCheck"> Remember me </label>
                </div>
                </div>

                <div class="col-md-6 d-flex justify-content-center">
                <a href="#!" style="color: #1E5128;">Forgot password?</a>
                </div>
            </div> -->

            <!-- Register buttons -->
            <div class="text-center ">
                <button type="submit" class="btn btn-success mb-4 col-lg-4" name = "login" id = "login"><i class='fa fa-sign-in'></i> Sign in</button>
                <p class="mb-4">Belum Daftar Akun? <a href="#!" style="color: ;">Register</a></p>
            </div>
            </form>
        </div>
        </div>
        <!-- Pills content -->
    </div>
  </div>
</body>
</html>