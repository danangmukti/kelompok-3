<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Optimus | Games</title>
    <link rel="icon" href="plant.png" style="width: 100%;">
    <link rel="stylesheet" href="css/games.css">
    <!-- Boxicons -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- My Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

</head>
    <body>
        <div class="sidebar">
        <div class="logo_content">
            <div class="logo">
                <img src="img/logo-icon.png" alt="">
            </div>
            <i class='bx bx-menu' id="btn"></i>
        </div>
        <div class="nav_list">
            <div class="list">
                <i class='bx bx-search' ></i>
                <input type="text" placeholder="Search..."></input>
                <span class="tooltip">Search</span>
            </div>
            <div class="list">
                <a href="./index.php">
                    <i class='bx bx-grid-alt' ></i>
                    <span class="links_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </div>
        </div>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <img src="img/profile1.png" alt="">
                    <div class="name_job">
                        <div class="name">Danang</div>
                        <div class="job">Developer</div>
                    </div>
                </div>
            </div>
            <i class='bx bx-log-out' id="log_out" ></i>
        </div>
        </div>

        <div class="container">
            <div class="wrapper">
            <div class="stats-container">
                <div id="moves-count"></div>
                <div id="time"></div>
            </div>
            <div class="game-container"></div>
            <button id="stop" class="hide">Stop Game</button>
            </div>
            <div class="controls-container">
            <p id="result"></p>
            <button id="start">Start Game</button>
            </div>
        </div>
        <!-- game js -->
        <script src="js/games.js"></script>
    </body>
</html>