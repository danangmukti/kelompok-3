<?php $conn = mysqli_connect("localhost", "root", "", "optimus");

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

function registrasi($data)
{
    global $conn;

    $email = htmlspecialchars($data["email"]);
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);


    $hasil = mysqli_query($conn, "SELECT email FROM akun WHERE email = '$email'");

    if (mysqli_fetch_assoc($hasil)) {
        echo "<script>
        alert('email sudah terdaftar!')
    </script>";

        return false;
    }

    //cek konfirmasi password
    if ($password !== $password2) {
        echo "<script>
            alert('konfirmasi password tidak sesuai!')
        </script>";
        return false;
    }

    //enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    //tambahkan userbaru ke database
    mysqli_query($conn, "INSERT INTO akun VALUES('','$email','$password')");

    return mysqli_affected_rows($conn);
}

function pesan($kirim)
{
    global $conn;
    $email = htmlspecialchars($kirim["email"]);
    $nama = htmlspecialchars($kirim["nama"]);
    $keluhan = htmlspecialchars($kirim["keluhan"]);
    $konsumsi = htmlspecialchars($kirim["konsumsi"]);
    $gejala = htmlspecialchars($kirim["gejala"]);

    //query insert data
    $query = "INSERT INTO konsul VALUE ('','$email','$nama', '$keluhan','$konsumsi','$gejala') ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

